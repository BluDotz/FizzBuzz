/*
 * Top module for iCEstick blinky
 * 
 * Flash horizontal red LEDs on Fizz (3) and vertical red LEDs on Buzz (5)
 *
 */

module top(input CLK
	   , output FizzRightLED
	   , output BuzzBottomLED
	   , output FizzLeftLED
	   , output BuzzTopLED
	   , output HeartBeatLED
	   );

   // PLL to get 100.5MHz clock
   wire       sysclk;
   wire       locked;
   pll myPLL (.clock_in(CLK), .global_clock(sysclk), .locked(locked));

   // 27-bit counter: 100.5MHz / 2^27 ~ 0.749Hz 
   localparam SYS_CNTR_WIDTH = 27;
   
   wire heartbeat;
   // assign heartbeat = syscounter[SYS_CNTR_WIDTH-1:SYS_CNTR_WIDTH-1];
   assign heartbeat = syscounter[SYS_CNTR_WIDTH-1];
   
   // .. use slowest to flash green LED,
   assign HeartBeatLED = heartbeat;

   reg [SYS_CNTR_WIDTH-1:0] syscounter;

   always @(posedge sysclk)
     syscounter <= syscounter + 1;


   count3 myThree (.theClock(heartbeat), .leftLED(FizzLeftLED), .rightLED(FizzRightLED));

   count5 myFive (.theClock(heartbeat), .topLED(BuzzTopLED), .bottomLED(BuzzBottomLED));
   
endmodule		 


module count3(input theClock, output leftLED, output rightLED);

   reg [1:0] counter;

   assign leftLED = (counter == 3);
   assign rightLED = leftLED;

   always @(posedge theClock)
      begin
            counter <= counter + 1;

            if (counter == 3) begin
               counter <= 1;
            end
      end

endmodule


module count5(input theClock, output topLED, output bottomLED);

   reg [2:0] counter;

   assign topLED = (counter == 5);
   assign bottomLED = topLED;

   always @(posedge theClock)
      begin
            counter <= counter + 1;

            if (counter == 5) begin
               counter <= 1;
            end
      end

endmodule
